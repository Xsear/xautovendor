
-- ------------------------------------------
-- xAutoVendor
--   by: Xsear
-- ------------------------------------------

require "lib/lib_Debug"
require "lib/lib_Callback2"
require "lib/lib_ChatLib"
require "lib/lib_InterfaceOptions"

-- ------------------------------------------
-- CONSTANTS
-- ------------------------------------------

c_AddonInfo = {
    component = select(1, Component.GetInfo()),
    name = select(2, Component.GetInfo()),
    release  = "2016-05-15",
    version = "1.0",
    patch = "prod-1962",
    save = 1,
}
c_OutputPrefix = "[xAutoVendor] "
c_AmmoPackTypeId = 30298
c_CrystiteTypeId = 10

c_ReasonableQuantity = 200

-- Xsear
--[[
d_ShoppingList = {
    [124652] = {label="Epic Weapon Cache", limit=c_ReasonableQuantity},
    [124653] = {label="Epic Ability Cache", limit=c_ReasonableQuantity},
    [124654] = {label="Epic Module Cache", limit=2000},
    [138334] = {label="Epic Armor Cache", limit=c_ReasonableQuantity},
    [140057] = {label="Pilot\'s Experience Pack", limit=50},
}
--]]

-- Rackloose

d_ShoppingList = {
    --[124652] = {label="Epic Weapon Cache", limit=c_ReasonableQuantity},
    --[124653] = {label="Epic Ability Cache", limit=c_ReasonableQuantity},
    --[124654] = {label="Epic Module Cache", limit=2000},
    --[138334] = {label="Epic Armor Cache", limit=c_ReasonableQuantity},
    --[140057] = {label="Pilot\'s Experience Pack", limit=2000},
    [c_AmmoPackTypeId] = {label="ammo", limit=1000}
}


-- ------------------------------------------
-- GLOBALS
-- ------------------------------------------

g_ActiveVendorId = nil
g_RemoteVendorId = nil
g_ActiveProducts = {}
g_SelectedProductTypeId = false
g_SelectedProductId = false
g_SelectedProductCurrencyId = false
g_PurchaseCycle = nil


-- ------------------------------------------
-- OPTIONS
-- ------------------------------------------

g_Loaded = false
g_Options = {}
g_Options.Enabled = true
g_Options.Debug = false
g_Options.MaxAmmoPacks = 500
g_Options.PurchaseCycleInterval = 1

-- InterfaceOptions Configuration
do
    InterfaceOptions.NotifyOnLoaded(true)
    InterfaceOptions.NotifyOnDefaults(true)
    InterfaceOptions.NotifyOnDisplay(true)
    InterfaceOptions.SaveVersion(c_AddonInfo.save)

    InterfaceOptions.AddCheckBox({id = "Enabled", label = "Enable addon", default = g_Options.Enabled})
    InterfaceOptions.AddCheckBox({id = "Debug", label = "Enable debug", tooltip = "Log messages into the console that are helpful to the addon creator in order to track down problems.", default = g_Options.Debug})
    --InterfaceOptions.AddSlider({id = "MaxAmmoPacks", label = "NOT IN USE Ammo Pack Limit", tooltip = "The addon will not purchase ammo packs when at or above this limit.", default = g_Options.MaxAmmoPacks, min = 50, max = 9001, inc = 5, suffix = "x"})
    InterfaceOptions.AddSlider({id = "PurchaseCycleInterval", label = "PurchaseCycleInterval", tooltip = "The rate at which the addon will issue purchase requests.", default = g_Options.PurchaseCycleInterval, min = 0.1, max = 5, inc = 0.1, suffix = "s"})
end

-- InterfaceOptions Updates
function OnOptionChanged(id, value)

    -- InterfaceOptions Notifications
    if id == "__LOADED" then
        OnOptionsLoaded()
        return
    elseif id == '__DEFAULT' then
        return
    elseif id == '__DISPLAY' then
        return
    end

    -- Update value
    g_Options[id] = value

    -- Debug
    if id == "Debug" then
        Component.SaveSetting("Debug", value)
        Debug.EnableLogging(value)
        return
    end

    -- Addon disabled
    if id == "Enabled" and not value then
        StopPurchasing()
    end

    -- Changed purchase interval
    if id == "PurchaseCycleInterval" then
        if g_PurchaseCycle then
            g_PurchaseCycle:Stop()
            g_PurchaseCycle:Run(tonumber(value))
        end
    end
end

-- InterfaceOptions Loaded
function OnOptionsLoaded()
    g_Loaded = true
end


-- ------------------------------------------
-- LOADING EVENTS
-- ------------------------------------------

function OnComponentLoad()
    -- Debug
    Debug.EnableLogging(Component.GetSetting("Debug"))

    -- Options
    InterfaceOptions.SetCallbackFunc(OnOptionChanged)
end


-- ------------------------------------------
-- GENERAL EVENTS
-- ------------------------------------------

function OnVendorInfoResponse(args)
    if not g_Options.Enabled then return end
    if args.vendor_id then
        -- Set active vendor
        g_ActiveVendorId = args.vendor_id
        
        -- Get vendor data
        local vendorProductList = Game.GetVendorProductList(g_ActiveVendorId)

        -- Must be careful here, we might not get info back
        if not vendorProductList then
            Debug.Log("Nil response from GetVendorProductList")

            -- Request it
            local requested = Game.RequestVendorProductList(g_ActiveVendorId)
            if not requested then 
                -- We were rejected, how embarassing :(
                Debug.Warn("RequestVendorProductList rejected :( VendorID: " .. tostring(g_ActiveVendorId))
            else
                -- This function will be called again when the game returns data
                Debug.Log("RequestVendorProductList success")
            end

            -- As we don't have product info, we cannot continue yet.
            Output("Waiting for the vendor to present his goods...")
            return
        end

        -- Store vendor data
        g_RemoteVendorId = vendorProductList.id
        g_ActiveProducts = vendorProductList.products

        Debug.Table("g_ActiveProducts", g_ActiveProducts)

        -- Reset active product data in case we have previous data
        ResetSelectedProduct()

        Debug.Log("A vendor has been selected and variables have been reset")

        -- Delegate a bit
        CheckVendorProducts()
    end
end

function OnTerminalAuth(args)
    if args and args.terminal_type == "VENDOR" and args.terminal_id then
        OnVendorInfoResponse({vendor_id=args.terminal_id}) -- Fake an event to kick it going again
    else
        StopPurchasing()
    end
end


-- ------------------------------------------
-- GENERAL FUNCTIONS
-- ------------------------------------------

function ResetSelectedProduct()
    g_SelectedProductId = false
    g_SelectedProductCurrencyId = false
    g_SelectedProductTypeId = false
end


function CheckVendorProducts()
        -- Find a relevant product on this vendor
        for _, product in pairs(g_ActiveProducts) do
            ResetSelectedProduct()
            local entry = d_ShoppingList[product.item_sdb_id]
            if entry then
                Debug.Log("Vendor lists an item that is on our shopping list.")

                if GetCurrentQuantityByTypeId(product.item_sdb_id) < entry.limit then
                    Debug.Log("We are below our limit, we wish to purchase.")

                    -- Find crystite price
                    for _, price in pairs(product.prices) do
                        if price.currency_remote_id == c_CrystiteTypeId then
                            g_SelectedProductCurrencyId = price.id
                            Debug.Log("Found g_SelectedProductCurrencyId: ", g_SelectedProductCurrencyId)
                            break
                        end
                    end

                    -- Only select product if we found a crystite price to use
                    if g_SelectedProductCurrencyId then
                        g_SelectedProductId = product.id
                        g_SelectedProductTypeId = product.item_sdb_id
                        Debug.Log("Found g_SelectedProductId: ", g_SelectedProductId)
                        
                        -- We have selected a product, end the loop.
                        break
                    end

                    Debug.Log("Something didn\'t check out, no deal.")
                else
                    Debug.Log("We have enough of that product already.")
                end
            end
        end

        -- Did we find product, indicating so that we can buy it?
        if g_SelectedProductId and g_SelectedProductCurrencyId and g_SelectedProductTypeId then
            Debug.Table("Stat", {
                g_ActiveVendorId = g_ActiveVendorId,
                g_RemoteVendorId = g_RemoteVendorId,
                g_SelectedProductId = g_SelectedProductId,
                g_SelectedProductCurrencyId = g_SelectedProductCurrencyId,
                g_SelectedProductTypeId = g_SelectedProductTypeId,
            })
            StartPurchasing()
        end
end

function RequestPurchase()
    assert(g_SelectedProductId ~= nil)
    assert(g_SelectedProductCurrencyId ~= nil)
    assert(g_RemoteVendorId ~= nil)
    assert(g_ActiveVendorId ~= nil)
    Debug.Table("RequestPurchase", {
        g_SelectedProductId = g_SelectedProductId,
        g_SelectedProductCurrencyId = g_SelectedProductCurrencyId,
        g_RemoteVendorId = g_RemoteVendorId,
        g_ActiveVendorId = g_ActiveVendorId,
    })
    Game.RequestVendorPurchase(g_SelectedProductId, g_SelectedProductCurrencyId, g_RemoteVendorId, g_ActiveVendorId)
end

function GetCurrentNumberOfSelectedProduct()
    return Player.GetItemCount(g_SelectedProductTypeId)
end

function GetCurrentQuantityByTypeId(typeId)
    return Player.GetItemCount(typeId)
end


function StartPurchasing()
    Debug.Log("StartPurchasing")
    -- Start cycle if enabled
    if g_Options.Enabled then
        if g_PurchaseCycle == nil then
            g_PurchaseCycle = Callback2.CreateCycle(PurchaseCycle)
            g_PurchaseCycle:Run(tonumber(g_Options.PurchaseCycleInterval))
            Output("Restocking on " .. tostring(d_ShoppingList[g_SelectedProductTypeId].label))
        end
    end
end

function StopPurchasing()
    Debug.Log("StopPurchasing")
    -- Destroy cycle if it exists
    if g_PurchaseCycle ~= nil then
        g_PurchaseCycle:Stop()
        g_PurchaseCycle:Release()
        g_PurchaseCycle = nil
        Output("Taking a break")
    end
end

function PurchaseCycle()
    -- Termination check
    if not g_Options.Enabled or not (GetCurrentNumberOfSelectedProduct() < d_ShoppingList[g_SelectedProductTypeId].limit) then
        Output("Welcome to the limit!")
        StopPurchasing()
        CheckVendorProducts()
        return
    end

    -- Order a purchase
    RequestPurchase()
end


-- ------------------------------------------
-- UTILITY/RETURN FUNCTIONS
-- ------------------------------------------

function Output(text)
    local args = {
        text = c_OutputPrefix .. tostring(text),
    }

    ChatLib.SystemMessage(args);
end